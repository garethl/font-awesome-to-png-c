﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Reflection;
using NDesk.Options;

namespace font_awesome_to_png
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            string color = "#000000";
            string filename = null;
            bool help = false;
            bool list = false;
            int size = 16;
            int padding = 4;
            string font = "fontawesome-webfont.ttf";

            var p = new OptionSet
                {
                    {"font=", "Font file to use (default: fontawesome-webfont.ttf)", v => font = v},
                    {"color=", "Color (HTML color code or name, default: black)", v => color = v},
                    {"filename=", "The name of the output file (it must end with \".png\"). If all files are exported, it is used as a prefix.", v => filename = v},
                    {"size=", "Size of image (default: 16)", (int v) => size = v},
                    {"padding=", "Padding around image to use (default: 4)", (int v) => padding = v},
                    {"list", "List available icon names and exit", v => list = v != null},
                    {"h|?|help", "Displays this help text", v => help = v != null},
                };
            var icons = p.Parse(args);

            Console.Error.WriteLine("font-awesome-to-png v{0}", Assembly.GetExecutingAssembly().GetName().Version.ToString(3));
            Console.Error.WriteLine();

            if (help || args.Length == 0)
            {
                Console.Error.WriteLine("Usage: ");
                Console.Error.WriteLine(@"font-awesome-to-png     [-h] [--color COLOR] [--filename FILENAME]
                       [--font FONT] [--list] [--size SIZE] [--margin MARGIN]
                       icon [icon ...]
");
                Console.Error.WriteLine("Options: ");

                p.WriteOptionDescriptions(Console.Error);

                Environment.ExitCode = 1;
                return;
            }

            if (list)
            {
                foreach (var icon in Icons.Values.Keys.OrderBy(x => x))
                    Console.WriteLine(icon);

                return;
            }

            if (icons.Count == 0)
            {
                Console.Error.WriteLine("WARNING: No icons specified for output");
                Environment.ExitCode = 1;
                return;
            }

            var fontFamily = LoadFont(font);
            if (fontFamily == null)
                return;
            
            Color colorColor = ParseColor(color);
            if (colorColor == Color.Transparent)
                return;

            IEnumerable<string> iconsToExport = icons;
            bool isSingle = false;
            if (icons.Count == 1 && "ALL".Equals(icons[0], StringComparison.OrdinalIgnoreCase))
            {
                iconsToExport = Icons.Values.Keys;
            }
            else if (icons.Count == 1)
            {
                isSingle = true;
            }

            foreach (var icon in iconsToExport)
            {
                char iconChar;
                if (Icons.Values.TryGetValue(icon, out iconChar))
                {
                    var iconFilename = isSingle ? (filename ?? icon + ".png") : (filename + icon + ".png");

                    Console.Error.WriteLine("Exporting icon \"{0}\" as {1} ({2}x{2} pixels, {3} padding)", icon, iconFilename, size, padding);

                    ExportIcon(fontFamily, size, iconChar, icon, iconFilename, colorColor, padding);
                }
                else
                {
                    Console.Error.WriteLine("ERROR: Unknown icon \"{0}\"", icon);
                }
            }
        }

        private static Color ParseColor(string color)
        {
            try
            {
                return ColorTranslator.FromHtml(color);
            }
            catch
            {
                try
                {
                    return ColorTranslator.FromHtml("#" + color);

                }
                catch (Exception)
                {
                    Console.Error.WriteLine("Unable to parse \"{0}\" as a color", color);
                    Environment.ExitCode = 1;
                    return Color.Transparent;
                }
            }
        }

        private static FontFamily LoadFont(string font)
        {
            if (!File.Exists(font))
            {
                Console.Error.WriteLine("Unable to find font file {0}", font);
                Environment.ExitCode = 1;
                return null;
            }

            try
            {
                //load the font
                var fonts = new PrivateFontCollection();
                fonts.AddFontFile(Path.GetFullPath(font));
                return fonts.Families[0];
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error loading font file: {0}", ex.Message);
                Environment.ExitCode = 1;
                return null;
            }
        }

        private static void ExportIcon(FontFamily fontFamily, int size, char icon, string name, string filename, Color color, int padding = 4)
        {
            var iconString = new string(icon, 1);
            const int multiplier = 10;
            var largeSize = size*multiplier;

            using (var brush = new SolidBrush(color))
            using (var largeImage = new Bitmap(largeSize, largeSize, PixelFormat.Format32bppArgb))
            {
                Rectangle largeBounds;
                using (var g = Graphics.FromImage(largeImage))
                {
                    SetGraphicsProperties(g);

                    //find the correct font size
                    using (var font = FindFont(g, fontFamily, iconString, largeSize))
                    {
                        g.DrawString(iconString, font, brush, 1, 1);
                        largeBounds = largeImage.GetTrimBounds();
                    }
                }

                int sizeWithoutPadding = size - padding;

                double x = 0;
                double y = 0;
                double width = sizeWithoutPadding;
                double height = sizeWithoutPadding;

                double ratio = largeBounds.Width/(double) largeBounds.Height;

                const int adjust = 0;

                if (ratio > 1) //wide image
                {
                    height = (sizeWithoutPadding - adjust)/ratio;
                    x = 0;

                    y = (sizeWithoutPadding - adjust - height)/2;
                    width = sizeWithoutPadding - adjust;
                }
                else if (ratio < 1) //tall image
                {
                    width = (sizeWithoutPadding - adjust)*ratio;

                    x = (sizeWithoutPadding - adjust - width)/2;
                    y = 0;
                    height = sizeWithoutPadding - adjust;
                }

                var destRect = new RectangleF((float) x + (padding/2), (float) y + (padding/2), (float) width, (float) height);

                using (var image = new Bitmap(size, size, PixelFormat.Format32bppArgb))
                {
                    using (var g = Graphics.FromImage(image))
                    {
                        SetGraphicsProperties(g);

                        g.DrawImage(largeImage, destRect, largeBounds, GraphicsUnit.Pixel);
                    }

                    image.Save(filename, ImageFormat.Png);
                }
            }
        }

        private static void SetGraphicsProperties(Graphics g)
        {
            g.CompositingQuality = CompositingQuality.HighQuality;
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            g.SmoothingMode = SmoothingMode.HighQuality;
            g.TextRenderingHint = TextRenderingHint.AntiAlias;
            g.PixelOffsetMode = PixelOffsetMode.HighQuality;
        }

        private static Font FindFont(Graphics g, FontFamily fontFamily, string icon, int size)
        {
            float closest = size*2;
            float closestSize = size;

            for (float i = size*.5f; i <= size; i += 1f)
            {
                using (var font = new Font(fontFamily, i))
                {
                    var result = g.MeasureString(icon, font);

                    var dX = size - result.Width;
                    var dY = size - result.Height;

                    if (dX < 0 || dY < 0)
                        break;

                    var minDiff = Math.Min(dX, dY);

                    if (minDiff < closest)
                    {
                        closest = minDiff;
                        closestSize = i;
                    }
                    else if (minDiff > closest)
                    {
                        break;
                    }
                }
            }

            return new Font(fontFamily, closestSize);
        }
    }
}