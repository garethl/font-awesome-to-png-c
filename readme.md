Font Awesome to PNG (C# version)
================================

This is a c# clone of the python version located at [https://github.com/odyniec/font-awesome-to-png](https://github.com/odyniec/font-awesome-to-png). 
It supports all the same options, with an additional "padding" option.

This program allows you to extract the awesome
[Font Awesome](http://fortawesome.github.com/Font-Awesome/) icons as PNG images
of specified size.

The Font Awesome TTF file is included, but newer versions can be sourced from the
[Font Awesome Github repository](https://github.com/FortAwesome/Font-Awesome).

### Usage

    font-awesome-to-png [-h] [--color COLOR] [--filename FILENAME]
                           [--font FONT] [--list] [--size SIZE]
                           icon [icon ...]

    positional arguments:
      icon                 The name(s) of the icon(s) to export (or "ALL" for
                           all icons)

    optional arguments:
      --color COLOR        Color (HTML color code or name, default: black)
      --filename FILENAME  The name of the output file (it must end with
                           ".png"). If all files are exported, it is used as a
                           prefix.
      --font FONT          Font file to use (default: fontawesome-webfont.ttf)
      --list               List available icon names and exit
      --size SIZE          Icon size in pixels (default: 16)
	  --padding PADDING    Padding around image to use (default: 4)

### Examples

Export the "play" and "stop" icons as 24x24 pixels images:

    font-awesome-to-png --size 24 play stop

Export the asterisk icon as 32x32 pixels image, in blue:

    font-awesome-to-png --size 32 --color blue asterisk

Export all icons as 16x16 pixels images:

    font-awesome-to-png ALL

### Notes

The software draws the icons on a 10x sized canvas to the resulting output, and then scales it down.
This results in a higher quality resulting image.